package de.ifgi.lodnasa.tutorial;

import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.geom.Line;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.*;
import gov.nasa.worldwind.util.BasicDragger;
import gov.nasa.worldwindx.examples.ApplicationTemplate;
import gov.nasa.worldwind.render.PointPlacemark;
import gov.nasa.worldwind.render.PointPlacemarkAttributes;

import java.util.ArrayList;
import java.util.LinkedList;

public class Poligono extends ApplicationTemplate
{
    public static class AppFrame extends ApplicationTemplate.AppFrame
    {
        public AppFrame()
        {
            super(true, true, false);
            //this.getWwd()addSelectListener(new BasicDragger(this.getWwd()));

            //Create the layer where you will place your polygons
            RenderableLayer layer = new RenderableLayer();

            // Set the basic attributes of your polygon
            ShapeAttributes normalAttributes = new BasicShapeAttributes();
            normalAttributes.setInteriorMaterial(Material.GREEN);
            normalAttributes.setOutlineWidth(1); 
            normalAttributes.setInteriorOpacity(0.4);
            normalAttributes.setOutlineOpacity(0.2);
            normalAttributes.setDrawInterior(true);
            normalAttributes.setDrawOutline(true);

            // Establecer las coordenadas (en grados) para dibujar su pol�gono
            ArrayList positions = new ArrayList();
            positions.add(Position.fromDegrees(-40.564355315219, -73.14851942062, 2e2));
            positions.add(Position.fromDegrees(-40.561463695881, -73.14978542327, 2e2));
            positions.add(Position.fromDegrees(-40.557573806083, -73.134786510, 2e2));
            positions.add(Position.fromDegrees(-40.559350758312, -73.134679222, 2e2));
            positions.add(Position.fromDegrees(-40.562692604919, -73.141030693, 2e2));
            positions.add(Position.fromDegrees(-40.566392894068, -73.1454295158, 2e2));
                          
            Polygon poly = new Polygon(positions);
            poly.setAttributes(normalAttributes);
                     
            //Tooltip text of the polygon
            poly.setValue(AVKey.DISPLAY_NAME, "Sector 1"); 
            
            //Marker
            Position positionMarker = Position.fromDegrees(-40.55759260491, -73.1456306);
            PointPlacemark Marker = new PointPlacemark(positionMarker);
            Marker.setValue(AVKey.DISPLAY_NAME, "Marker 1 - Posicion: " + Marker.getPosition());
            layer.addRenderable(poly);
            layer.addRenderable(Marker);
            layer.getMinActiveAltitude();
                       
            // Add the layer (capa) to the model.
            insertBeforeCompass(getWwd(), layer);
            // Update layer panel
            this.getLayerPanel().update(this.getWwd());
        }
    }

    public static void main(String[] args)
    {
        //Set the initial configurations of your NASA World Wind App
        //Altitute, logitude and latitute, and window caption.
        Configuration.setValue(AVKey.INITIAL_LATITUDE, -40.560051744842056);
        Configuration.setValue(AVKey.INITIAL_LONGITUDE, -73.14364852905274);
        Configuration.setValue(AVKey.INITIAL_ALTITUDE, 5e3);
        ApplicationTemplate.start("NASA World Wind", AppFrame.class);
    }
}