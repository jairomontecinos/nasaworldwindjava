package de.ifgi.lodnasa.tutorial;

import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.LatLonGraticuleLayer;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.PointPlacemark;
import gov.nasa.worldwindx.examples.ApplicationTemplate;

public class Map2 extends ApplicationTemplate
{
    public static class AppFrame extends ApplicationTemplate.AppFrame
    {
        public AppFrame()
        {
            super(true, true, false);
          
            // Add graticule
            insertBeforePlacenames(this.getWwd(), new LatLonGraticuleLayer());
   		
    		// Update layer     
            this.getLayerPanel().update(this.getWwd());
               
        }
    }

    public static void main(String[] args)
    {
        ApplicationTemplate.start("Started with NASA World Wind", AppFrame.class);
       
    }
}