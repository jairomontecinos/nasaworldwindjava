import java.awt.Color;
import java.util.LinkedList;
import java.util.ArrayList;
import gov.nasa.worldwind.BasicModel;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.Polyline;
import javax.swing.JFrame;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.render.PointPlacemark;
import gov.nasa.worldwind.util.BasicDragger;
import gov.nasa.worldwind.layers.LatLonGraticuleLayer;
import gov.nasa.worldwindx.examples.ApplicationTemplate;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class Test {
	
	public static void main(String[] args) {

		//create a WorldWind main object
		WorldWindowGLCanvas wwd = new WorldWindowGLCanvas();
		//wwd.setModel(new BasicModel());
		wwd.addSelectListener(new BasicDragger(this.getWwd()));
	
		//build Java swing interface
		JFrame frame = new JFrame("World Wind Map");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(wwd);
		frame.setSize(800,600);
		frame.setVisible(true);
				
		//create some "Position" to build a polyline
		LinkedList<Position> list = new LinkedList<Position>();
		for(int i = 0 ; i < 90 ; i++) {
			list.add(Position.fromDegrees(i,0.0,i));
		}
		//////////marker
		Position pointPosition = Position.fromDegrees(-40.560051744842056, -73.14364852905274);
		PointPlacemark pmStandard = new PointPlacemark(pointPosition);
		pmStandard.setValue(AVKey.DISPLAY_NAME, "Text Displayed by mouse-over");
		//RenderableLayer layer = new RenderableLayer();
		
		
		
		//create "Polyline" with list of "Position" and set color / thickness
		Polyline polyline = new Polyline(list);
		polyline.setColor(Color.RED);
		polyline.setLineWidth(3.0);
		
		//Poligono
		ArrayList positions = new ArrayList();

		positions.add(Position.fromDegrees(52, 10, 5e4));
		positions.add(Position.fromDegrees(55, 11, 5e4));
		positions.add(Position.fromDegrees(55, 11, 5e4));
		positions.add(Position.fromDegrees(52, 14, 5e4));
		positions.add(Position.fromDegrees(52, 10, 5e4));
		
		
		//create a layer and add Polyline
		RenderableLayer layer = new RenderableLayer();
		layer.addRenderable(polyline);
		layer.addRenderable(pmStandard);
		
		//add layer to WorldWind
		//wwd.getModel().getLayers().add(layer);
		 // Add the layer to the model.
        insertBeforeCompass(getWwd(), layer);
        // Update layer panel
        this.getLayerPanel().update(this.getWwd());
	}
	
}