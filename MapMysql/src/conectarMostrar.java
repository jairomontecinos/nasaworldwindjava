import java.sql.*;
 
public class conectarMostrar {
    private static Connection conexion = null;
    private static String bd = "montechm_montech2"; 
    private static String user = "root"; 
    private static String password = ""; 
    // Driver para MySQL en este caso.
    private static String driver = "com.mysql.jdbc.Driver";
    // Ruta del servidor.
    private static String server = "jdbc:mysql://localhost/" + bd;
 
    public static void main(String[] args) throws SQLException {
 
        System.out.println("INICIO DE EJECUCI�N.");
        conectar();
        Statement st = conexion();
        String Con = "";
   
        // Se crean datos de prueba para utilizarlos en la tabla 
       // Con = "INSERT INTO table_name (`id`, `Nombre`, `Apellido`, `Telefono`, `Email`) VALUES (1, 'Jose', 'Mart�nez', '11111111', 'jose@gmail.com');";
        //consultaActualiza(st, Con);
 
        // Se sacan los datos de la tabla ganado
        Con = "SELECT * FROM Ganado;";
        ResultSet rs = consultaQuery(st, Con);
        if (rs != null) {
            System.out.println("El listado de animales es el siguiente:");
            //System.out.println("  Resultado: ");
            while (rs.next()) {
                System.out.println("  ID: " + rs.getObject("id"));
                System.out.println("  Nombre: "
                        + rs.getObject("nom") + " Temp: "
                        + rs.getObject("temp"));
 
                System.out.println("  Peso: " + rs.getObject("peso")
                        + " Litros: " + rs.getObject("lts"));
 
                System.out.println("- ");
            }
            cerrar(rs);
        }
        cerrar(st);
        System.out.println("FIN DE EJECUCI�N.");
    }
 
    /**
     * M�todo necesario para conectarse al Driver y poder usar MySQL.
     */
    public static void conectar() {
        try {
            Class.forName(driver);
            conexion = DriverManager.getConnection(server, user, password);
        } catch (Exception e) {
            System.out.println("Error: Imposible realizar la conexion a BD.");
            e.printStackTrace();
        }
    }
 
    /**
     * M�todo para establecer la conexi�n con la base de datos.
     * @return
     */
    private static Statement conexion() {
        Statement st = null;
        try {
            st = conexion.createStatement();
        } catch (SQLException e) {
            System.out.println("Error: Conexi�n incorrecta.");
            e.printStackTrace();
        }
        return st;
    }
 
    /**
     * M�todo para realizar consultas del tipo: SELECT * FROM tabla WHERE..."
     * @param st
     * @param Con La consulta en concreto
     * @return
     */
    private static ResultSet consultaQuery(Statement st, String Con) {
        ResultSet rs = null;
        try {
            rs = st.executeQuery(Con);
        } catch (SQLException e) {
            System.out.println("Error con: " + Con);
            System.out.println("SQLException: " + e.getMessage());
            e.printStackTrace();
        }
        return rs;
    }
 
    /**
     * M�todo para realizar consultas de actualizaci�n, creaci�n o eliminaci�n.
     * @param st
     * @param Con = La consulta en concreto
     * @return
     */
    private static int consultaActualiza(Statement st, String Con) {
        int rs = -1;
        try {
            rs = st.executeUpdate(Con);
        } catch (SQLException e) {
            System.out.println("Error con: " + Con);
            System.out.println("SQLException: " + e.getMessage());
            e.printStackTrace();
        }
        return rs;
    }
 
    /**
     * M�todo para cerrar la consula
     * @param rs
     */
    private static void cerrar(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (Exception e) {
                System.out.print("Error: No es posible cerrar la consulta.");
            }
        }
    }
 
    /**
     * M�todo para cerrar la conexi�n.
     * @param st
     */
    private static void cerrar(java.sql.Statement st) {
        if (st != null) {
            try {
                st.close();
            } catch (Exception e) {
                System.out.print("Error: No es posible cerrar la conexi�n.");
            }
        }
    }
}