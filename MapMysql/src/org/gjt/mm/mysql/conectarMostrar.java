
import java.sql.*;
 
public class conectarMostrar {
    private static Connection conexion = null;
    private static String bd = "agenda"; // Nombre de BD.
    private static String user = "root"; // Usuario de BD.
    private static String password = ""; // Password de BD.
    // Driver para MySQL en este caso.
    private static String driver = "com.mysql.jdbc.Driver";
    // Ruta del servidor.
    private static String server = "jdbc:mysql://localhost/" + bd;
 
    public static void main(String[] args) throws SQLException {
 
        System.out.println("INICIO DE EJECUCI�N.");
        conectar();
        Statement st = conexion();
 
        // Se elimina la tabla "personal" en caso de existir.
        String cadena = "DROP TABLE IF EXISTS personal;";
        consultaActualiza(st, cadena);
 
        // Se crea la tabla "personal"
        cadena = "CREATE TABLE personal (`Identificador` int(11) NOT NULL AUTO_INCREMENT, `Nombre` varchar(50) NOT NULL, `Apellidos` varchar(50) NOT NULL, `Telefono` varchar(9) DEFAULT NULL, `Email` varchar(60) DEFAULT NULL, PRIMARY KEY (`Identificador`))";
        consultaActualiza(st, cadena);
 
        // Se crean datos de prueba para utilizarlos en la tabla "personal"
        cadena = "INSERT INTO personal (`Identificador`, `Nombre`, `Apellidos`, `Telefono`, `Email`) VALUES (1, 'Jos�', 'Mart�nez L�pez', '968112233', 'jose@martinezlopez.com'), (2, 'Mar�a', 'G�mez Mu�oz', '911876876', 'maria@gomezoliver.com'), (3, 'Juan', 'S�nchez Fern�ndez', '922111333', 'juan@sanchezfernandez.com'), (4, 'Ana', 'Murcia Rodr�guez', '950999888', 'ana@murciarodriguez.com');";
        consultaActualiza(st, cadena);
 
        // Se sacan los datos de la tabla personal
        cadena = "SELECT * FROM personal;";
        ResultSet rs = consultaQuery(st, cadena);
        if (rs != null) {
            System.out.println("El listado de persona es el siguiente:");
 
            while (rs.next()) {
                System.out.println("  ID: " + rs.getObject("Identificador"));
                System.out.println("  Nombre completo: "
                        + rs.getObject("Nombre") + " "
                        + rs.getObject("Apellidos"));
 
                System.out.println("  Contacto: " + rs.getObject("Telefono")
                        + " " + rs.getObject("Email"));
 
                System.out.println("- ");
            }
            cerrar(rs);
        }
        cerrar(st);
        System.out.println("FIN DE EJECUCI�N.");
    }
 
    /**
     * M�todo neecesario para conectarse al Driver y poder usar MySQL.
     */
    public static void conectar() {
        try {
            Class.forName(driver);
            conexion = DriverManager.getConnection(server, user, password);
        } catch (Exception e) {
            System.out.println("Error: Imposible realizar la conexion a BD.");
            e.printStackTrace();
        }
    }
 
    /**
     * M�todo para establecer la conexi�n con la base de datos.
     *
     * @return
     */
    private static Statement conexion() {
        Statement st = null;
        try {
            st = conexion.createStatement();
        } catch (SQLException e) {
            System.out.println("Error: Conexi�n incorrecta.");
            e.printStackTrace();
        }
        return st;
    }
 
    /**
     * M�todo para realizar consultas del tipo: SELECT * FROM tabla WHERE..."
     *
     * @param st
     * @param cadena La consulta en concreto
     * @return
     */
    private static ResultSet consultaQuery(Statement st, String cadena) {
        ResultSet rs = null;
        try {
            rs = st.executeQuery(cadena);
        } catch (SQLException e) {
            System.out.println("Error con: " + cadena);
            System.out.println("SQLException: " + e.getMessage());
            e.printStackTrace();
        }
        return rs;
    }
 
    /**
     * M�todo para realizar consultas de actualizaci�n, creaci�n o eliminaci�n.
     *
     * @param st
     * @param cadena La consulta en concreto
     * @return
     */
    private static int consultaActualiza(Statement st, String cadena) {
        int rs = -1;
        try {
            rs = st.executeUpdate(cadena);
        } catch (SQLException e) {
            System.out.println("Error con: " + cadena);
            System.out.println("SQLException: " + e.getMessage());
            e.printStackTrace();
        }
        return rs;
    }
 
    /**
     * M�todo para cerrar la consula
     *
     * @param rs
     */
    private static void cerrar(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (Exception e) {
                System.out.print("Error: No es posible cerrar la consulta.");
            }
        }
    }
 
    /**
     * M�todo para cerrar la conexi�n.
     *
     * @param st
     */
    private static void cerrar(java.sql.Statement st) {
        if (st != null) {
            try {
                st.close();
            } catch (Exception e) {
                System.out.print("Error: No es posible cerrar la conexi�n.");
            }
        }
    }
}